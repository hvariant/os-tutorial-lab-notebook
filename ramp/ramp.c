#include <linux/module.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/cdev.h>

struct cdev ramp_cdev;
dev_t ramp_dev;

#define BUFFER_SIZE 250

static int size = BUFFER_SIZE;
static char alphabet[255];

static int ramp_show(struct seq_file *m, void *v)
{
	seq_printf(m, "%s\n", alphabet);
	return 0;
}

static ssize_t ramp_write(struct file *_file, const char __user *buf, size_t count, loff_t *offs){
    int bytes_written;
    int bytes_to_write;
    int maxbytes = size - *offs;
    if(maxbytes > count) 
        bytes_to_write = count;
    else
        bytes_to_write = maxbytes;

    bytes_written = bytes_to_write - copy_from_user(alphabet + *offs, buf, bytes_to_write);
    printk("[ramp]: device has been written %d\n",bytes_written);

    *offs += bytes_written;
    alphabet[*offs] = 0;

    return bytes_written;
}

static int ramp_open(struct inode *inode, struct file *file)
{
	return single_open(file, ramp_show, NULL);
}

long ramp_ioctl(struct file * file, unsigned int cmd, unsigned long arg)
{
    printk("[ramp]: cmd=%u, arg=%lu\n",cmd,arg);

    if(cmd == 0){
        if(arg < 0 || arg > BUFFER_SIZE){
            return -EINVAL;
        }

        size = arg;
        memset(alphabet+size,0,BUFFER_SIZE-size);
    }
    
    return 0;
}

static const struct file_operations ramp_fops = {
    .owner      = THIS_MODULE,
	.open		= ramp_open,
	.read		= seq_read,
    .write      = ramp_write,
	.llseek		= seq_lseek,
	.release	= single_release,
    .unlocked_ioctl      = ramp_ioctl,
};

static int __init ramp_init(void)
{
    char temp[20];

    strcpy(alphabet,"Thequickbrownfoxjumpsoverthelazydog");

    printk("[ramp]:+ramp_init\n");
    alloc_chrdev_region(&ramp_dev,0,1,"ramp");
    printk("[ramp]:ramp_dev = %s\n", format_dev_t(temp, ramp_dev));

    cdev_init(&ramp_cdev, &ramp_fops);
    ramp_cdev.owner = THIS_MODULE;
    cdev_add(&ramp_cdev, ramp_dev, 1);

    return 0;
}

static void __exit ramp_exit(void)
{
    printk("[ramp]:+ramp_exit\n");
    unregister_chrdev_region(ramp_dev,1);
    cdev_del(&ramp_cdev);
}

module_init(ramp_init);
module_exit(ramp_exit);
