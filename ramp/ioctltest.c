#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>

int main(void){
//    int ioctl(int fd, unsigned long request, ...);
    int fd = open("/dev/ramp",O_WRONLY);
    int r=0;

    if(fd < 0){
        printf("failed to open ramp file\n");
        return 1;
    }

    r = ioctl(fd,0,10);
    if(r != 0){
        printf("ioctl failed\n");
        return 1;
    }

    return 0;
}
