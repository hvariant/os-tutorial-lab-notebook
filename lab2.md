# Lab 2

Step 1:

To write to out proc entry from user space we need to implement the ```.write``` field of ```file_operations```
when we pass the ```struct file_operations``` to proc_create, similar to:

- http://lxr.free-electrons.com/source/fs/proc/base.c?v=4.4#L565

For a basic implementation of the write interface, refer to:
http://www.tldp.org/LDP/lkmpg/2.6/html/x769.html

Step 2:

To fix the include statements, add ```#include <linux/module.h>``` at the beginning of [hello.c](hello_module/hello.c).

Step 3:

Added ```printk("hello world!\n");``` at the beginning of the definition of ```hello_proc_show``` in [hello.c](hello_module/hello.c).

Step 4:

- Changed the second parameter of the call to ```proc_create``` to 644 so that root user can write to the proc entry.
- The ```hello_proc_write``` implementation in [hello2.c](hello_module/hello2.c) is similar to [the implementation](http://www.tldp.org/LDP/lkmpg/2.6/html/x769.html) referenced in step 1.
- Set the ```.write``` field of ```hello_proc_fops``` to ```hello_proc_write``` in [hello2.c](hello_module/hello2.c).

To be more specific, in the parameters of ```hello_proc_write```,
```buf``` is a pointer to the user input string, ```count``` is the size of the buffer,
to copy from user space to kernel space ```copy_from_user``` needs to be used.
The return value of ```copy_from_user``` is the number of bytes not successfully copied,
to simplify if the return value is not zero we simply return error code ```-EFAULT``` if we failed to copy everything.

To avoid overflow, we copy at most ```hello_buff_length``` number of bytes and discard the rest by returning ```count``` in `hello_proc_write` when ```copy_from_user``` is successful so as to terminate the I/O operation.

See [this code](hello_module/hello2.c) for more detail.

Step 5:

We simply make ```hello_buff``` a char pointer, call ```kmalloc``` in ```proc_hello_init```, and call ```kfree``` in ```cleanup_hello_module```.

See [this code](hello_module/hello.c) for more detail.

Step 6:

- Declare ```int blackhole_count;``` in [blackhole.c](hello_module/blackhole.c), and then use ```EXPORT_SYMBOL(blackhole_count)``` to export this symbol.
- Use ```extern int blackhole_count;``` at the beginning of [blackholecount.c](hello_module/blackholecount.c) to use this variable.

See [blackhole.c](hello_module/blackhole.c) and [blackholecount.c](hello_module/blackholecount.c) for more detail.