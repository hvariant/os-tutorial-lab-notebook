#include  <stdio.h>
#include <unistd.h>

int main(void)
{

     if(fork() == 0){
         printf("Hello world!\n");
     } else {
         //parent process
         return 0;
     }

     return 0;
}

