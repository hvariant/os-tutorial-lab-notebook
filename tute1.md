# Tutorial 1

1. What is an operating system?

    An OS is the software of a computer system that sits between hardware and user applications and provides an environment where programs can execute.
    It can manage resources and provides high-level interfaces for user programs to utilize those resources.

2. What are some characteristics of a good operating system. In some cases these characteristics can be conflicting. Give some examples of possible conflict?

    Convenience, efficiency, ability to evolve, security, reliability, low power-consumption.
    Convenience and efficiency, for instance, the user might want better graphical interfaces but these features would take up resources and make the OS less efficient.
    Efficiency and security, by separating user mode and kernel mode there is more security but it incurs more overhead when user programs need to access system resources by system calls.

3. Are libraries part of the operating system? How about distribution management software?

    No and no. Both are user programs that use interfaces provided by the OS and are not part of it.

4. Give some examples of space-multiplexed and time-multiplexed resources. As resource demand increases what is the effect on a time-multiplexed resource? Is this the same for a space-multiplexed resource?

    Space-multiplexed resources: hard disk, memory. Time-multiplexed resources: CPU.
    Effects on time-multiplexed resource: slower turnaround time.
    Effects on space-multiplexed resource: run out of space.

5. Early computer systems used a sign-up or reservation scheme. How did this work? What were its advantages? Why was it abandoned?

    A user submit a sign up sheet and the operators would allocate each of the user requests to certain time slots.

    Advantages:
        - Programmers can utilize resources of the whole computer system.

    Disadvantages:
        - If a user finishes her job early the computer would be idle for the rest of the time alloted to her.
        - If a user runs out of time she'll have to submit another sign up sheet and book another time, increasing set up time.

6. How does off-line operation of I/O devices improve performance?

    It concurrently converts slower I/O into faster I/O.
    As an example, we can concurrently convert deck of cards to and from tape with satellite processing systems while running the tape through the main computer.

7. Explain buffering and spooling. Suppose we have 5 similar jobs, each job taking approximately 2 seconds of CPU time. Also, each job performs output of exactly 100 lines to the single line printer (That is each job has 1/50 seconds of CPU time before outputting a line). Suppose the printer outputs one line per second. How long will it take to complete all the jobs if:

    Buffering is used to overlap I/O with computation.
    For input, data is automatically read and stored in a buffer while computation is taking place.
    When the job requires the data it may read directly from the buffer.
    For output data is placed into a buffer and than output to the device when it is able.

    In spooling (Simultaneous Peripheral Operation On-Line) cards are read directly onto a disk. Later when the job is ran input is taken directly from the disk and output is given directly to the disk.
    Once the job is complete the output is sent directly from the disk drive to the printer.
    Spooling allows the I/O of one job to overlap with the computation of another job.
    Also jobs would not be delayed during execution due to slow I/O devices.

  * neither buffering or spooling is employed,

    (1/50 + 1)*100*5 //compute for 1/50s, print one line. repeat 100 times for each job.
    = 510

  * buffering is employed (10 line buffer, assume the buffer is cleared before the next job starts), or

    //!the CPU and the printer can read/write from/to the buffer at the same time

    For each job:

             #1    #2-#10  #11   #12       #100
    CPU      1/50  1/50*9  1/50  1/50 ...  1/50
    Printer        1       2     3    ...  91   92-100
                   #1      #2    #3        #91  #92-#100
    
    (1/50 + 100)*5 = 500.1
    
  * spooling is employed(the I/O time for the disk used for spooling is negligible) ?

    //start printing after job is done

            J1  J2  J3  J4  J5
    CPU     2   2   2   2   2
    Printer     100             ... 100

    2+100*5 = 502

    How do these change if we have two line printers and we output the first two jobs to one printer and the last three to the other printer?

             J1  J2  J3  J4  J5
    CPU      2   2   2   2   2
    Printer1     100             100
    Printer2             100          100       100
    
    6 + 100*3 = 306

8. It is possible to construct a secure operating system without a privileged mode of operation in hardware. Explain how this is possible? What is the advantage of providing such a privileged mode of operation in hardware?

    Yes. Simulate hardware with privileged mode (Turing completeness).
        (Or: by not allowing users/programmers to import code from somewhere else; by implementing a custom instruction set, a la sandbox.)

    Main advantage (virtualization): efficiency by direct execution; dual mode provides virtual environment provides constraints on programs.

9. Why are interrupts so useful in modern computer systems?

    Hardware/software interrupt:
        - more efficient than polling I/O.
        - allow user programs to be interrupted so that the OS can deal with urgent events
        - provide a secure way for user programs to make system calls
        - can be used in error handling.

10. Suppose you had a large contiguous block of data to transfer from primary memory to a device via the system bus. Would DMA be able to transfer the data faster than programmed I/O (if yes by how much, if not why not)? While transferring this block using DMA would the performance of the code executing on the CPU be effected? State any assumptions you make.

    Assume a simplistic model:

    Scenario #1

    CPU   Memory  Device
     |      |      |
     |      |      |        DMA
     |      |      |         |
     |      |      |         |
    ========================================================== System bus

    Same speed

    Scenario #2

    CPU   Memory  Device
     |      |      |
     |      |      |-DMA
     |      |      |        
     |      |      |        
    ========================================================== System bus

    Faster, directly from memory to device.

    Second part of the question:

    DMA will have little impact on CPU execution (http://electronics.stackexchange.com/questions/212254/does-a-cpu-completely-freeze-when-using-a-dma):

        Answer 1:

        You are correct that the CPU cannot be accessing the memory during a DMA transfer.
        However there are two factors which in combination allow apparent parallel memory access by the CPU and the device performing the DMA transfer:

        The CPU takes multiple clock cycles to execute an instruction.
        Once it has fetched the instruction, which takes maybe one or two cycles, it can often execute the entire instruction without further memory access (unless it is an instruction which itself access memory, such as a mov instruction with an indirect operand).
        The device performing the DMA transfer is significantly slower than the CPU speed, so the CPU will not need to halt on every instruction but just occasionally when the DMA device is accessing the memory.

        In combination, these two factors mean that the device performing the DMA transfer will have little impact on the CPU speed.

        EDIT: Forgot to mention that there's also the factor of CPU cache, which as long as the code that the CPU is executing is in the cache then it won't need to access real memory to fetch instructions, so a DMA transfer is not going to get in the way (although if the instruction needs to access memory then obviously a real memory access will take place - potentially having to wait for a break in the DMA device's use of the memory).

        -------------------------------------

        Answer 2:

        If there is a single memory interface, there would be hardware to arbitrate between requests.
        Typically a processor would be given priority over I/O without starving I/O, but even with I/O always having priority the processor would have some opportunities to access memory because I/O tends to have lower bandwidth demands and to be intermittent.

        In addition, there is typically more than one interface to memory.
        Higher performance processors typically have caches (if DMA is not coherent, the caches do not even have to be snooped; even with snooping, overhead would generally be small because of the bandwidth difference between cache and main memory or (when the DMA transfers to L3 cache) between L3 cache and L1 cache), providing a separate interface to access memory.
        Microcontrollers will often access instructions from a separate flash-based memory, allowing fetch to proceed during DMA to on-chip memory, and often have tightly coupled memory with an independent interface (allowing many data accesses to avoid DMA conflicts).
        
        Even with a single memory interface, the peak bandwidth will generally be higher than the bandwidth typically used. (For instruction fetch, even a small buffer with wider than average fetch loading from memory would allow instruction fetch from the buffer while another agent is using the memory interface, exploiting the tendency of code not to branch.)

        Also note that because a processor accesses data, if there is a single memory interface, there must be a mechanism for arbitration between data accesses and instruction accesses.

        If the processor (with a single memory interface) was forced to implement a copy from an I/O device buffer to main memory, it would also have to fetch instructions to perform the copy.
        This could mean two memory accesses per word transferred even in an ISA with memory-memory operations (a load-store ISA could require three memory accesses or more if post-increment memory addressing is not provided);
        that is in addition to the I/O access which in old systems might share the same interface as main memory.
        A DMA engine does not access instructions in memory, and so avoids this overhead.

11. What is the distinction between mechanism and policy in operating system design. Give an example.

    Mechanism: how something is done.
    Policy: a decision about what to do.

    Example:
        - Mechanism: user file access control such as file permission, owner, quota, etc.
        - Policy: let root access everything.

12. What registers does a typical IO port consists of? What different approaches are used to access these registers from the processor?

    Registers: data I/O, status, control.
    
    Approaches (basically):
        - memory-mapped I/O: access device registers as if accessing ordinary memory
        - special I/O instructions

