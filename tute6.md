# Tutorial 6

1. What is the difference between security and protection?

    Security is the degree of resistance to, or protection from, harm.
    Protection is the mechanism which controls access to system resources.

2. Explain how passwords are stored and authenticated within UNIX systems.

    store both the hash and salt.
    when authenticating, use both password input and salt to produce the hash and
    compare the hash with the stored hash.

3. [Review Q15.14 from Stallings] "Explain the difference between conventional encryption and public-key encryption."

    conventional encryption is symmetric, use the same key for encryption & decryption so you need the key in order to encrypt data.

    public-key encryption is asymmetric, to encrypt data you only need the public key, to decrypt data you need private key.

4. [Problems 15.11 from Stallings] "Suppose that someone suggests the following way to confirm that the two of you are both in possession of the same secret key. You create a random bit string the length of the key, XOR it with the key, and send the result over the channel. Your partner XORs the incoming block with the key, and send the result over the channel. Your partner sends it back. You check and if what you receive is your original random string, you have verified that your partner has the same secret key, yet neither of you has ever transmitted the key. Is there a flaw in the scheme?"

    man in the middle can intercept the two transmissions and XOR them to get the secret key.

5. Given a disk drive has 500 cylinders. These are
numbered 0 to 499. The drive is currently serving a request at cylinder 14, and the previous request was at cylinder 12. The queue of requests(in order) are:
```
8, 147, 91, 177, 94, 150, 102, 175, 13
```
For each of the following algorithms, what is the total distance(in cylinders) that the head moves to satisfy all of the requests? (start from the current head position)

    * FCFS:
        14 -> 8: 6
        8 -> 147: 139
        147 -> 91: 56
        91 -> 177: 86
        177 -> 94: 83
        94 -> 150: 56
        150 -> 102: 48
        102 -> 175: 73
        175 -> 13: 162
        
        total = 709 //answer in class is wrong!

    * SSTF:
        14 -> 8: 6
        8 -> 13: ...
        13 -> 91: ...
        91 -> 94: ...
        94 -> 102: ...
        102 -> 147: ...
        147 -> 150: ...
        150 -> 175: ...
        175 -> 177: ...
        
        total = 6 + 177-8 = 175

    * SCAN:
        14 -> 91 -> ... -> 177: 163
        177 -> 13 -> 8: 169
        
        total = 332

    * C-SCAN:
        14 -> 91 -> ... -> 177: 163
        177 -> 8: 169 //jump back to the other end
        8 -> 13: 5 //work in the same direction
        
        total = 337

6. Explain how SSTF may starve some requests.

    if all new requests are close to the current location, previous requests that are further might be starved.

7. Which cylinders does SSTF tend to favour? Why does it favour these cylinders?

    the middle cylinders. because all middle cylinder requests will be served on the way to the end of the disk.
