# Lab 3

### S1

[mmap](http://man7.org/linux/man-pages/man2/mmap.2.html): map or unmap files or devices into virtual memory.

[mprotect](http://man7.org/linux/man-pages/man2/mprotect.2.html): changes the protection flag on a region of memory.

[signal](http://man7.org/linux/man-pages/man2/signal.2.html): signals are notifications sent to processes to notify that an event has occured. it's a form of IPC. use signal(2) to register a signal handler so that a process can handle specific signals using that handler. [sigaction](http://man7.org/linux/man-pages/man2/sigaction.2.html) is recommended instead of signal for portability.

vfs makes it simpler for people to implement filesystems because it provides a layer of abstraction and a set of interfaces so developers only need to implement the required interfaces
and there's no need to consider the low-level details about how the OS manages file systems internally.

### S2

[mmap_copy.c](Lab3/mmap_copy.c)

### S3

memalign is obsolete, used [aligned_alloc](http://linux.die.net/man/3/memalign) instead.

[sigaction](http://man7.org/linux/man-pages/man2/sigaction.2.html) is recommended instead of [signal](http://man7.org/linux/man-pages/man2/signal.2.html) for portability.

[mprotect_test.c](Lab3/mprotect_test.c)

### S4


* printk output:

  - `ls`:
```
[ 3692.143007] vvsfs - readdir
[ 3692.143018] vvsfs - readblock : 0
[ 3692.143020] vvsfs - readblock done : 0
[ 3692.143032] vvsfs - readdir
[ 3692.143033] vvsfs - readblock : 0
[ 3692.143033] vvsfs - readblock done : 0
```

  - `ls file1`:
```
[ 3707.952136] vvsfs - lookup
[ 3707.952141] vvsfs - readblock : 0
[ 3707.952143] vvsfs - readblock done : 0
[ 3707.952150] vvsfs - readdir
[ 3707.952151] vvsfs - readblock : 0
[ 3707.952152] vvsfs - readblock done : 0
[ 3707.952161] vvsfs - readdir
[ 3707.952162] vvsfs - readblock : 0
[ 3707.952162] vvsfs - readblock done : 0
[ 3707.952171] vvsfs - lookup
[ 3707.952172] vvsfs - readblock : 0
[ 3707.952173] vvsfs - readblock done : 0
[ 3708.100885] vvsfs - readdir
[ 3708.100889] vvsfs - readblock : 0
[ 3708.100890] vvsfs - readblock done : 0
[ 3708.100906] vvsfs - readdir
[ 3708.100907] vvsfs - readblock : 0
[ 3708.100907] vvsfs - readblock done : 0
```

  - `cat file1`:
```
[ 3786.721784] vvsfs - readdir
[ 3786.721788] vvsfs - readblock : 0
[ 3786.721790] vvsfs - readblock done : 0
[ 3786.721796] vvsfs - readdir
[ 3786.721797] vvsfs - readblock : 0
[ 3786.721798] vvsfs - readblock done : 0
[ 3787.893471] vvsfs - file read - count : 131072 ppos 0
[ 3787.893474] vvsfs - readblock : 2
[ 3787.893477] vvsfs - readblock done : 2
[ 3787.893486] vvsfs - file read - count : 131072 ppos 2
[ 3787.893487] vvsfs - readblock : 2
[ 3787.893488] vvsfs - readblock done : 2
```

  - `touch file3`:
```
[ 3832.347913] vvsfs - lookup
[ 3832.347917] vvsfs - readblock : 0
[ 3832.347919] vvsfs - readblock done : 0
[ 3832.347920] vvsfs - create : file3
[ 3832.347921] vvsfs - new inode
[ 3832.347922] vvsfs - readblock : 0
[ 3832.347923] vvsfs - readblock done : 0
[ 3832.347923] vvsfs - readblock : 1
[ 3832.347924] vvsfs - readblock done : 1
[ 3832.347925] vvsfs - readblock : 2
[ 3832.347926] vvsfs - readblock done : 2
[ 3832.347927] vvsfs - readblock : 3
[ 3832.347927] vvsfs - readblock done : 3
[ 3832.347928] vvsfs - writeblock : 3
[ 3832.347979] vvsfs - writeblock done: 3
[ 3832.347981] vvsfs - readblock : 0
[ 3832.347982] vvsfs - readblock done : 0
[ 3832.347983] vvsfs - writeblock : 0
[ 3832.348003] vvsfs - writeblock done: 0
```

  - `echo "abfdsjafdasf" > file3`
```
[ 3883.902162] vvsfs - readdir
[ 3883.902167] vvsfs - readblock : 0
[ 3883.902169] vvsfs - readblock done : 0
[ 3883.902178] vvsfs - readdir
[ 3883.902179] vvsfs - readblock : 0
[ 3883.902179] vvsfs - readblock done : 0
[ 3884.351122] vvsfs - file write - count : 13 ppos 0
[ 3884.351125] vvsfs - readblock : 3
[ 3884.351128] vvsfs - readblock done : 3
[ 3884.351129] vvsfs - writeblock : 3
[ 3884.351168] vvsfs - writeblock done: 3
[ 3884.351169] vvsfs - file write done : 13 ppos 0
```

* read from a file: `vvsfs_readdir`, `vvsfs_file_read`, `vvsfs_readblock`

* writing to a file: `vvsfs_readdir`, `vvsfs_file_write`, `vvsfs_readblock`, `vvsfs_writeblock`

* listing a directory: `vvsfs_readdir`, `vvsfs_readblock`

* creating a new file:  `vvsfs_create`, `vvsfs_new_inode`, `vvsfs_readdir`, `vvsfs_create`, `vvsfs_readblock`, `vvsfs_writeblock`

* what happens when you attempt to remove a file?

  `rm: cannot remove 'file1': Operation not permitted`

* 'cd' to the directory you mounted the vvsfs on and determine the number of bytes this direcory takes up. What is odd about this result?

  The directory shouldn't take 0 bytes.

### S5

In [vvsfs.c](vvsfs/vvsfs.c) around line 293:

```
dir->i_size++;
mark_inode_dirty(dir);
```

### S6
