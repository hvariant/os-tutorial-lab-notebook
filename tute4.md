# Tutorial 4

1. Given three processes P1, P2, and P3, each process has a section of code R1, R2, and R3 respectively. The code section R3 may only commence after both R1 and R2 have completed their execution. Also, R1 and R2 must be executed mutually exclusively. Use binary semaphores to ensure the proper execution of the three processes.

    //initialize
    sem lock=1, finish1=0, finish2=0;

    //P1
    wait(lock);
    { R1; }
    signal(lock);
    signal(finish1);

    //P2
    wait(lock);
    { R2; }
    signal(lock);
    signal(finish2);

    //P3
    wait(finish1);
    wait(finish2);
    { R3; }

2. What is the distinction between deadlock prevention and deadlock avoidance? Give some examples of different approaches used for deadlock prevention.

    ref: https://www.cs.jhu.edu/~yairamir/cs418/os4/tsld011.htm

    * deadlock prevention: eliminate necessary conditions under which deadlock may occur by constraining how resource requests can be made and how they are handled (system design).

    * deadlock avoidance:
        - arbitrary request but dynamically consider whether it is safe to grant request atm
        - requires additional a priori information regarding the overall potential use of each resource for each process.

    Similar to the difference between a traffic light and a police officer directing traffic.

    ref: http://www.personal.kent.edu/~rmuhamma/OpSystems/Myos/deadlockPrevent.htm

    Example: 
        - circular wait: apply a total ordering of resources, only request resources using that total ordering
        - hold and wait: request everything in one go

        (* - banker's algorithm is a deadlock avoidance algorithm *)

3. The resource allocation graph: //see drawing on notebook

   * initially: Work = (0,1), Finish = (false, false, false)

   * after one iteration: Work = (0,0), Finish = (false, false, true)

   process 1 & 2 are deadlocked.
