# Lab 4

## Step 1

* use SYSCALL_DEFINEx (x being the number of parameters) to define the system call
(c.f. [getpid](http://lxr.free-electrons.com/source/kernel/sys.c?v=4.4#L830) and [unlink](http://lxr.free-electrons.com/source/fs/namei.c?v=4.4#L3915))

* add the system call to the [system call table](http://lxr.free-electrons.com/source/arch/x86/entry/syscalls/syscall_64.tbl?v=4.4)

When a user program makes a system call, it pushed the parameters to the stack and then inserts a trap to set the IP to
the kernel system call handler, which based on the system call number directs control to the appropriate system call function
and then puts the return value to the return register and set the IP back to the user program.

Ref: https://tssurya.wordpress.com/2014/08/19/adding-a-hello-world-system-call-to-linux-kernel-3-16-0/

## Step 2

in [fs/namei.c](http://lxr.free-electrons.com/source/fs/namei.c?v=4.4), add:

```
static char* dont_delete_filename = NULL;
SYSCALL_DEFINE1(dont_delete,const char __user *, filename){
    //...
    
    return 0;
}
```

### Step 2 TODO

move the name check to `may_delete` from `do_unlinkat`

## Step 3

at the end of [the syscall table](http://lxr.free-electrons.com/source/arch/x86/entry/syscalls/syscall_64.tbl?v=4.4):

```
400     common  dont_delete                  sys_dont_delete
```

## Step 4

Same as lab1.

## Step 5

Using [`syscall`](http://man7.org/linux/man-pages/man2/syscall.2.html) in [test_syscall.c](test_syscall.c):

```
#define _GNU_SOURCE         /* See feature_test_macros(7) */
#include <unistd.h>
#include <sys/syscall.h>   /* For SYS_xxx definitions */

//...

syscall(400,argv[1]);

//...

```

## Step 6

Running `strace -o soutput ./test_syscall abcd` gives (in [soutput](soutput)):

```
syscall_400(0x7fff85cae28f, 0x7f4400f89780, 0x7fffffed, 0, 0x13, 0x7fff85cac218) = 0
```
