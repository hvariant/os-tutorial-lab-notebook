# Lab 5

## Step 1

In addition to the source provided in the lab instructions,
[this](http://www.crashcourse.ca/introduction-linux-kernel-programming/lesson-16-lets-talk-about-devices)
page is also helpful.

## Step 3

The main reference for this step is [this article](http://www.crashcourse.ca/introduction-linux-kernel-programming/lesson-18-simplest-possible-character-device-driver-you-can-wr).

Here we simply use seq_file to simplify implementation similar to lab2.

When initializing the kernel module, we should call `alloc_chrdev_region` to ask the kernel to allocate a major and minor number for our device:

    alloc_chrdev_region(&ramp_dev,0,1,"ramp");

We need to use `struct cdev` to manage our character device, and initialize the struct:

    cdev_init(&ramp_cdev, &ramp_fops);
    ramp_cdev.owner = THIS_MODULE;
    cdev_add(&ramp_cdev, ramp_dev, 1);

When removing the module we need:

    unregister_chrdev_region(ramp_dev,1);
    cdev_del(&ramp_cdev);

after installing ramp kernel module, the of `cat /proc/devices` includes `245 ramp` (245 is the major number).

## Step 4

`cat /proc/devices | grep ramp` shows that the major number for the device is 245.

we then call `sudo mknod /dev/ramp c 245 0` to create a device file

the output of `cat /dev/ramp` is:

    Thequickbrownfoxjumpsoverthelazydog

## Step 5

c.f. lab2.
