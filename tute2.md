# Tutorial 2

1. What are the advantages and disadvantage of a file system keeping tack of the type of each file?

    * advantages: 
        - optimize I/O
        - make access control policies, etc based on file type

    * disadvantages: lose flexibility, difficult to add new types

2. Why do most operating systems use open and close operations for files?

    * OS only need to check access when `open` & `close`

3. List some applications which access data in a file sequentially. List some which access data randomly.

    * sequentially: `cat`, `grep`, compilers

    * randomly: database systems, dynamic libraries

4. Suppose disk blocks are 512 bytes and block pointers are 32 bit. Construct an example that illustrates how the following allocation strategies would store a file of 1020 bytes.

    * contiguous: 

        block: #1       #2
        data:  [0-511] [512-1019]

    * linked:
    
        block: #1                ...    #10              ...   #30
        data   [0-507] [#10]     ...    [508-1015] [#30] ...   [1016-1019] [NULL]
                        ^
                        |
                        |
                      (pointer to next block)

    * FAT:

        In FAT:
            ...
            #100: #150
            ...
            #150: NULL

        block: ... #100       ...    #150
        data   ... [0-511]    ...    [512-1019]


    * indexed(single-level):

        Index block:
                |
                |
                V
        block: #50             ... #100       ...    #150
        data   [#100][#150]    ... [0-511]    ...    [512-1019]

5. How do caches improve the performance of a file system? What are some problems with using large caches?

    It allows the file system to reduce the number of actual reads and writes to the disk.

    Using large caches will increase system cost, and it can be more difficult to maintain consistency between cache & memory/disk.

6. What is the difference between a RAM disk and using a disk cache? Does the introduction of a disk cache make RAM disks redundant?

    A RAM disk reserves part of the system memory and uses that memory space as a disk and all the reads/writes to the RAM disk will be on the memory space of the RAM disk.

    A disk cache is a type of cache memory for writing/reading a disk.

    No, because you don't need to have a backup disk with a RAM disk, could be useful for liveCDs for instance.

7. What approaches can be taken to make a file system more robust?

    ideas from RAID:
        * keep multiple copies of data so if one copy is lost redundant copies can be used to recover

        * use some form of block interleaved parity so when for instance one block becomes bad the other blocks together with the parity block can be used to recover data in the bad block.

8. What is the maximum file size for files in:

    * ext2 with 4K blocks: 4K * (12 + 4K/4B + (4K/4B)^2 + (4K/4B)^3) ~= 4TB.

    * FAT32: the file length entry in the directory table is 32 bit, therefore the maximum file size is (2^32-1)B ~= 4GB
