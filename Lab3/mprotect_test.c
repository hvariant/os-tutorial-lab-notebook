#include <stdio.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <signal.h>

#define PAGE_SIZE 4096

void* mem;

void die(const char* s){
    fprintf(stderr,"%s\n",s);

    exit(1);
}


void mem_handler(int signum){
    printf("setting memory protection flag to PROT_WRITE in signal handler\n");
    mprotect(mem, PAGE_SIZE, PROT_WRITE);
}

int main(int argc, char* argv[]){
    struct sigaction action, old_action;

    action.sa_handler = mem_handler;
    sigemptyset (&action.sa_mask);
    action.sa_flags = 0;
    if(sigaction(SIGSEGV, &action, &old_action) < 0){
        die("failed to register signal handler");
    }

    mem = aligned_alloc(PAGE_SIZE, PAGE_SIZE);
    mprotect(mem, PAGE_SIZE, PROT_NONE);

    //attempt to write
    strcpy((char*)mem,"Hello world!");

    printf("finished writing!\n");
    printf("content of mem is now:%s\n", (char*)mem);

    return 0;
}
