#include <stdio.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <sys/mman.h>

void die(const char* s){
    fprintf(stderr,"%s\n",s);

    exit(1);
}

int main(int argc, char* argv[]){
    int src,dst;
    struct stat src_stats;
    long long fsize;
    void *ibuf,*obuf;

    if(argc != 3){
        die("usage: mmap_copy [input] [output]");
    }
    if((src = open(argv[1],O_RDONLY)) == -1){
        die("failed to open input file");
    }
    if((dst = open(argv[2],O_RDWR | O_CREAT | O_TRUNC, 00644)) == -1){
        die("failed to create output file");
    }

    if(fstat(src,&src_stats)){
        die("failed to get stats for input file");
    }
    fsize = src_stats.st_size;
    if(ftruncate(dst,fsize)){
        die("failed to set size for output file");
    }

    ibuf = mmap(NULL, fsize, PROT_READ, MAP_PRIVATE, src, 0);
    obuf = mmap(NULL, fsize, PROT_READ | PROT_WRITE, MAP_SHARED, dst, 0);

    if(ibuf == MAP_FAILED){
        die("failed to map input file to memory");
    }
    if(obuf == MAP_FAILED){
        die("failed to map output file to memory");
    }

    memcpy(obuf,ibuf,fsize);

    munmap(ibuf,fsize);
    munmap(obuf,fsize);

    return 0;
}
