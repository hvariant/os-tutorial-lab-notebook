#define _GNU_SOURCE         /* See feature_test_macros(7) */
#include <unistd.h>
#include <sys/syscall.h>   /* For SYS_xxx definitions */
#include <stdio.h>

int main(int argc, char* argv[]){
    if(argc != 2){
        fprintf(stderr,"need to specify filename\n");
        return 1;
    }
    printf("dont_delete: %s\n",argv[1]);

    syscall(400,argv[1]);

    return 0;
}
