# Tutorial 3

1. What is a process? Describe the difference between an algorithm, a program, and a process?

    A process is a program in execution.

    An algorithm describes an approach to solving a problem, but it's not grounded in a specific language.
    A program is a particular instantiation of an algorithm in a specific language, such as C or python.
    A process is a running program.

2. What information must be contained in the PCB (Process Control Block)?

    Stack, program counter, registers, open files, process state, process number, scheduling information, memory information, etc.

3. What is the difference/similarity between handling a interrupt and doing a context switch?

    ref: https://www.quora.com/Operating-Systems-What-is-the-difference-between-context-switching-and-interrupt-handling

    * similarity:
        - both stop/freeze the running process
        - both capture information so that the OS can restart the running process

    * difference:
        - with context switch the OS set up another process and shifts to that other process,
            so the OS needs to store additional information (such as memory information) related to the previous running process
        - with interrupts the OS needs to save less information and because after handling the interrupt the OS resumes to the previous running process,
            it only needs to store information that will be changed during interrupt handling (such as PC, registers).

4. What is a mid-term scheduler for? How is it different from a long-term scheduler and a short-term scheduler?

    ref: https://www.quora.com/What-is-long-term-scheduler-short-term-scheduler-and-mid-term-term-scheduler-in-OS

    - Mid-term schedulers move processes in and out of main memory. It is in charge of swapping and controls the degree of multiprogramming.
    - Long term scheduler decides which programs are admitted to the system for processing.
    - Short term scheduler decides which processes in the ready process queue to execute next.

5. Suppose you are given the task of designing a mid-term scheduler. How would you determine which processes to remove from main memory? When would you return processes to memory?

    When a process makes an I/O request it becomes suspended, so the scheduler can swap it out and make more space for other active processes in the main memory.
    When the I/O request is completed the process needs to be swapped back in.
    
    Or when the degree of multiprogramming is reduced.
    Large working set maybe?

6. Gantt charts: 
    
    notes:
    - new arrival at back of the queue by default (for RR)

7. In UNIX what do the fork and execve system calls do.

    [fork](https://linux.die.net/man/2/fork) creates a child process by duplicating the parent process.
    [execve](https://linux.die.net/man/2/execve) replaces the calling process with a new process that executes the program specified by the filename parameter.

8. Write a simple UNIX program that creates another process that prints ''Hello World''.

    [tut3_1.c](tut3_1.c)

9. Write a program that creates 5 threads. Each thread should print 10 times the id number for that thread. Add some delay between the printing of the id numbers and see how this changes the scheduling of the threads. Show a print out of your code to your tutor.

    [tut3_2.c](tut3_2.c)
