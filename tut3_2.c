#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

void *PrintHello(void *threadid)
{
   int i;
   long tid = (long)threadid;
   struct timespec req,rem;

   req.tv_sec = 0;
   req.tv_nsec = 1000000;
   for(i=0;i<10;i++){
       printf("Thread #%ld to ground control.\n", tid);
       nanosleep(&req,&rem);
   }

   pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
   pthread_t threads[5];

   int rc;
   long t;

   for(t=0;t<5;t++) pthread_create(&threads[t], NULL, PrintHello, (void *)t);

   for(t=0;t<5;t++) pthread_join(threads[t],NULL);

   /* Last thing that main() should do */
   pthread_exit(NULL);
}

