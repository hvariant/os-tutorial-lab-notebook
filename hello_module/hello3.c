
/* hello world module - Eric McCreath 2005,2006,2008,2010,2012 */
/* to compile use:
    make -C  /usr/src/linux-headers-`uname -r` SUBDIRS=$PWD modules
   to install into the kernel use :
    insmod hello.ko
   to test :
    cat /proc/hello
   to remove :
    rmmod hello
*/

#include <linux/module.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/slab.h>
#include <asm/uaccess.h>

#define GROWTH_FACTOR 2

static int buf_size = 100;
static int buf_len = 0;
static char* hello_buf = NULL;

static int hello_proc_show(struct seq_file *m, void *v)
{
    printk("hello3 world!\n");

	seq_printf(m, "%s", hello_buf);
	return 0;
}

static void realloc_buf(void){
    //alloc new buf
    char* new_buf = kmalloc(buf_size*2,GFP_KERNEL);
    memcpy(new_buf,hello_buf,buf_len);
    new_buf[buf_len] = 0;

    //update info
    buf_size *= 2;
    hello_buf = new_buf;
}

static ssize_t hello_proc_write(struct file *_file, const char __user *buf, size_t count, loff_t *offs){
    size_t max_offset = *offs + count;
    int bytes_remaining, bytes_written;
    while(max_offset > buf_size - buf_len - 10) realloc_buf();

    bytes_remaining = copy_from_user(hello_buf + *offs, buf, count);
    if(bytes_remaining < 0) return -EFAULT;
    bytes_written = count - bytes_remaining;

    *offs += bytes_written;
    hello_buf[*offs] = 0;

    return bytes_written;
}

static int hello_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, hello_proc_show, NULL);
}

static const struct file_operations hello_proc_fops = {
	.open		= hello_proc_open,
	.read		= seq_read,
    .write      = hello_proc_write,
	.llseek		= seq_lseek,
	.release	= single_release,
};

static int __init proc_hello_init(void)
{
    printk("init proc hello3\n");

    hello_buf = kmalloc(buf_size,GFP_KERNEL);
    *hello_buf = 0;

	proc_create("hello3", 644, NULL, &hello_proc_fops);
	return 0;
}
static void __exit cleanup_hello_module(void)
{
  remove_proc_entry("hello3",NULL);

  if(hello_buf)
      kfree(hello_buf);

  printk("cleanup proc hello\n");
}

module_init(proc_hello_init);
module_exit(cleanup_hello_module);
