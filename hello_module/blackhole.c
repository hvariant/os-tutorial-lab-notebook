#include <linux/module.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>

int blackhole_count;
EXPORT_SYMBOL(blackhole_count);

static int blackhole_proc_show(struct seq_file *m, void *v)
{
	seq_printf(m, "\n");
	return 0;
}

static ssize_t blackhole_proc_write(struct file *_file, const char __user *buf, size_t count, loff_t *offs){
    blackhole_count += count;
    return count;
}

static int blackhole_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, blackhole_proc_show, NULL);
}

static const struct file_operations blackhole_proc_fops = {
	.open		= blackhole_proc_open,
	.read		= seq_read,
    .write      = blackhole_proc_write,
	.llseek		= seq_lseek,
	.release	= single_release,
};

static int __init proc_blackhole_init(void)
{
    printk("init proc blackhole\n");

    blackhole_count = 0;
	proc_create("blackhole", 644, NULL, &blackhole_proc_fops);
	return 0;
}
static void __exit cleanup_blackhole_module(void)
{
    remove_proc_entry("blackhole",NULL);
    printk("cleanup proc blackhole\n");
}


module_init(proc_blackhole_init);
module_exit(cleanup_blackhole_module);
