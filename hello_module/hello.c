
/* hello world module - Eric McCreath 2005,2006,2008,2010,2012 */
/* to compile use:
    make -C  /usr/src/linux-headers-`uname -r` SUBDIRS=$PWD modules
   to install into the kernel use :
    insmod hello.ko
   to test :
    cat /proc/hello
   to remove :
    rmmod hello
*/

#include <linux/module.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/slab.h>
#include <asm/uaccess.h>

#define hello_buff_size 60
#define hello_max_length 50
char* hello_buff;

static int hello_proc_show(struct seq_file *m, void *v)
{
    printk("hello world!\n");

	seq_printf(m, "%s", hello_buff);
	return 0;
}

static ssize_t hello_proc_write(struct file *_file, const char __user *buf, size_t count, loff_t *offs){
    int N = count > hello_max_length ? hello_max_length : count;
    int r;

    memset(hello_buff,0,hello_buff_size);
    r = copy_from_user(hello_buff, buf, N);
    if(r != 0)
        return -EFAULT;

    return count;
}

static int hello_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, hello_proc_show, NULL);
}

static const struct file_operations hello_proc_fops = {
	.open		= hello_proc_open,
	.read		= seq_read,
    .write      = hello_proc_write,
	.llseek		= seq_lseek,
	.release	= single_release,
};

static int __init proc_hello_init(void)
{
    printk("init proc hello\n");

    hello_buff = kmalloc(hello_buff_size,GFP_KERNEL);

	proc_create("hello", 644, NULL, &hello_proc_fops);
	return 0;
}
static void __exit cleanup_hello_module(void)
{
  remove_proc_entry("hello",NULL);

  kfree(hello_buff);

  printk("cleanup proc hello\n");
}


module_init(proc_hello_init);
module_exit(cleanup_hello_module);
