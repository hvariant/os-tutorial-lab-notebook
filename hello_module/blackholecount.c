#include <linux/module.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>

extern int blackhole_count;

static int blackholecount_proc_show(struct seq_file *m, void *v)
{
	seq_printf(m, "%d\n", blackhole_count);
	return 0;
}

static int blackholecount_proc_open(struct inode *inode, struct file *file)
{
	return single_open(file, blackholecount_proc_show, NULL);
}

static const struct file_operations blackholecount_proc_fops = {
	.open		= blackholecount_proc_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};

static int __init proc_blackholecount_init(void)
{
    printk("init proc blackholecount\n");

	proc_create("blackholecount", 0, NULL, &blackholecount_proc_fops);
	return 0;
}
static void __exit cleanup_blackholecount_module(void)
{
    remove_proc_entry("blackholecount",NULL);
    printk("cleanup proc blackholecount\n");
}


module_init(proc_blackholecount_init);
module_exit(cleanup_blackholecount_module);
