# Tutorial 5

1. What are the main advantages of demand paging?

    Only load pages needed by the process to save memory space. Less loading related latency at program startup.

2. Why is it difficult to implement LRU for demand paging?

    Too much overhead for each memory reference.

3. Consider the following page reference string:
  ```
  1, 2, 3, 2, 7, 4, 5, 6, 7, 7, 3, 3, 2, 1, 2, 3, 6.  
  ```
  Circle the page references when a page fault occurs for: FIFO(3 frames available), FIFO(4 frames available), LRU(3 frames available), OPT(3 frames available). All the frames are initially empty. How do the number of page faults compare?

    * FIFO(3 frames): _1_ _2_ _3_ 2 _7_ _4_ _5_ _6_ _7_ 7 _3_ 3 _2_ _1_ 2 3 _6_

      12 PFs

    * FIFO(4 frames): _1_ _2_ _3_ 2 _7_ _4_ _5_ _6_ 7 7 _3_ 3 _2_ _1_ 2 3 6

      10 PFs

    * LRU(3 frames): _1_ _2_ _3_ 2 _7_ _4_ _5_ _6_ _7_ 7 _3_ 3 _2_ _1_ 2 3 _6_

      12 PFs

    * OPT(3 frames): _1_ _2_ _3_ 2 _7_ _4_ _5_ _6_ 7 7 3 3 _2_ _1_ 2 3 _6_

      10 PFs

4. From OSC Silberschatz Q9.5 page 331 "Suppose we have a demand-paged memory. The page table is held in registers. It takes 8 milliseconds to service a page fault if an empty page is available or the replaced page is not modified, and 20 milliseconds if the replaced page is modified. Memory access time is 100 nanoseconds.

  Assume that the page to be replaced is modified 70 percent of the time. What is the maximum acceptable page-fault rate for an effective access time of no more than 200 nanoseconds? "

    1 millisecond = 10^(-3) second
    1 microsecond = 10^(-6) second
    1 nanosecond  = 10^(-9) second

    100 * 10^(-9) * (1-r) + r * (100 * 10^(-9) + 8 * 10^(-3) * (1-30%) + 20 * 10^(-3) * 30%) <= 200 * 10^(-9)

    ===> r <= 6.098 * 10^(-6)

5. Explain thrashing. How may a system detect thrashing? What approaches could be used to deal with this problem once it is detected?

    when the working set is larger than available frames so the VM is constantly paging.
    reduce the degree of multiprogramming.

6. Explain the buddy approach for memory allocation. When is it useful within kernels?

    divide memory into smaller and smaller partitions (typically halves) and try to find the most smallest partition that can satisfy current request.
    it's useful because it's simple, easy to implement, fast, has little external fragmentation and easy compaction.
    it still has internal fragmentation but can be used alongside slab allocation to provide more fine-grained allocation.
