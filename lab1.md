# Lab 1

## Step 2

The definition is at 'fs/namei.c'.

unlink -> do_unlinkat(AT_FDCWD, path)

'do_unlinkat' deletes the name from the filesystem.

## Step 3

Because the content of '/proc/cpuinfo' is archetecture specific,
we need to modify 'arch/x86/kernel/cpu/proc.c', specifically the 'show_cpuinfo' function.

I changed [this statement](http://lxr.linux.no/#linux+v3.13/arch/x86/kernel/cpu/proc.c#L61) and inserted my name and student id into the string.

Then I ran 'make -j2', 'sudo make install'.

## Step 4

In ['do_unlinkat'](http://lxr.linux.no/#linux+v3.13/fs/namei.c#L3688), insert the following code:

    if(!strcmp(name->name,"dont_delete")) return -EPERM;
    
## Step 6

At 'hello_proc_show':

    #include<linux/random.h>
    
    //......

    char* greetings[] = {"Hi!", "Howdy", "G'day", "Greetings", "Grrr"};
    int i,n;
    get_random_bytes(&n,sizeof(n));
    i = abs(n) % 5;
    
    seq_printf(m, "%s\n", greetings[i]);
    
    //......